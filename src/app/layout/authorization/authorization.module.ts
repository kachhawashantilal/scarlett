import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoginComponent, SignupComponent } from 'src/app/components/authorization/authorization';
import { AuthorizationRoutingModule } from './authorization-routing.module';
import { AuthorizationComponent } from './authorization.component';



@NgModule({
  declarations: [
    AuthorizationComponent,
    LoginComponent,
    SignupComponent
  ],
  imports: [
    CommonModule,
    AuthorizationRoutingModule
  ]
})
export class AuthorizationModule { }
