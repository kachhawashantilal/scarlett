import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleLayoutRoutingModule } from './simple-layout-routing.module';
import { SimpleLayoutComponent } from './simple-layout.component';
import { SalonsComponent, BlogComponent, ContactUsComponent, HomeComponent } from 'src/app/components/simple-layout/simple-layout';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    SimpleLayoutComponent,
    SalonsComponent,
    BlogComponent,
    ContactUsComponent,
    HomeComponent,

  ],
  imports: [
    CommonModule,
    SimpleLayoutRoutingModule,
    SharedModule,
  ]
})
export class SimpleLayoutModule { }
