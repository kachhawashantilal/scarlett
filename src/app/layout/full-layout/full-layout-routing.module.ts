import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FullLayoutComponent } from './full-layout.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', loadChildren: () => import('../simple-layout/simple-layout.module').then(m => m.SimpleLayoutModule) },
      { path: 'login', loadChildren: () => import('../authorization/authorization.module').then(m => m.AuthorizationModule) },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FullLayoutRoutingModule { }
