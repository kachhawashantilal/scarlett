import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent, BlogListComponent, SalonsListComponent } from './shared';
import { JoinUsNowComponent } from '../directives/join-us-now/join-us-now.component';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [
    HeaderComponent,
    BlogListComponent,
    JoinUsNowComponent,
    FooterComponent,
    SalonsListComponent

  ],
  imports: [
    CommonModule,
  ],
  exports : [
    HeaderComponent,
    BlogListComponent,
    JoinUsNowComponent,
    FooterComponent,
    SalonsListComponent
  ]
})
export class SharedModule { }
